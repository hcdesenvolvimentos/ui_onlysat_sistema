$(function(){
	$('.menuVertical button.openMenu').click(function(e){
		$('.menuVertical').addClass('ativo');
		$('.menuVertical').removeClass('fechado');
		setTimeout(function(){
			$('.menuVertical').addClass('textVisible')
		},500);


	});

	$('.closeMenu').click(function(){
		$('.menuVertical').removeClass('ativo');
		$('.menuVertical').removeClass('textVisible');
		$('.menuVertical').addClass('fechado');
	});

	$('.menuVertical ul.itensPai li').click(function(){
		$(this).children().next().toggleClass('ativacted');

	});

	$('.modalComandos .conteudoModal .fecharModal').click(function(){
		$('.modalComandos').slideUp();
	});

	$('#openModalComandos').click(function(){
		$('.modalComandos').slideDown();
	});

	$('.pg-associar-eventos .tabelaPrincipal .listaEventos li').click(function(){
		let idTab = $(this).attr('id');
		$('.pg-associar-eventos .tabelaPrincipal .conteudosTabs .tab_content').removeClass('ativo');
		$('.pg-associar-eventos .tabelaPrincipal .conteudosTabs .tab_content#'+idTab).addClass('ativo');
	});

	$('.modal-relatorio-analitico span.fecharModal').click(function(){
		$('.modal-relatorio-analitico').slideUp('2000');
	});

	$('.ocultar-painel').click(function(){
		$('.informacoes-mapa').addClass('oculto');
		$('.exibirPainel').removeClass('oculto');
	});
	$('.exibirPainel').click(function(){
		$('.informacoes-mapa').removeClass('oculto');
		$('.exibirPainel').addClass('oculto');
	});

	$('.relatorio-analitico-tabela .table-responsive .table tbody tr').dblclick(function(){
		$('.modal-relatorio-analitico').fadeIn();
	});

	$('.menuSuperior .esquerdaMenuSuperior ul.listaMenuSuperior li i, .menuSuperior .esquerdaMenuSuperior ul.listaMenuSuperior li span').click(function(){
		$('.notificacoes').toggleClass('hide');
	});
});
